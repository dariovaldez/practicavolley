package facci.cuartoa.practicavolley;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class JsonObjectBusquedaActivity extends AppCompatActivity {

    EditText txtIdPersonaje;
    Button btnBuscarPersonaje;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_object_busqueda);
        txtIdPersonaje = (EditText) findViewById(R.id.txtIdPersonaje);
        btnBuscarPersonaje = (Button) findViewById(R.id.btnBuscarPersonaje);

        btnBuscarPersonaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), JsonObjectActivity.class);
                intent.putExtra("id", txtIdPersonaje.getText().toString());
                startActivity(intent);
            }
        });

    }
}
