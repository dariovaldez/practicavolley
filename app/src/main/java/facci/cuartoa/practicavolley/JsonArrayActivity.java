package facci.cuartoa.practicavolley;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import facci.cuartoa.practicavolley.Adapter.EpisodiosAdapter;
import facci.cuartoa.practicavolley.Objetos.Episodio;

public class JsonArrayActivity extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener{

    RecyclerView recyclerEpisodio;
    ArrayList<Episodio> listaEpisodio;
    ProgressDialog cargando;
    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_array);

        listaEpisodio = new ArrayList<>();
        recyclerEpisodio = (RecyclerView) findViewById(R.id.recyclerEpisodio);
        recyclerEpisodio.setLayoutManager(new LinearLayoutManager(this));
        recyclerEpisodio.setHasFixedSize(true);

        request = Volley.newRequestQueue(this);
        
        cargarEpisodios();

    }

    private void cargarEpisodios() {
        cargando = new ProgressDialog(this);
        cargando.setTitle("Cargando");
        cargando.setMessage("Esto podria tardar un momento");
        cargando.show();

        String url = "https://rickandmortyapi.com/api/episode";

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        cargando.hide();
        Toast.makeText(this, "No se pudo conectar al servidor", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        Episodio episodio = null;
        JSONArray json = response.optJSONArray("results");
        try {
            for (int i = 0; i < json.length(); i++){
                episodio = new Episodio();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                episodio.setId(jsonObject.optInt("id"));
                episodio.setName(jsonObject.optString("name"));
                episodio.setAir_date(jsonObject.optString("air_date"));
                episodio.setEpisode(jsonObject.optString("episode"));
                listaEpisodio.add(episodio);
            }
            cargando.hide();
            EpisodiosAdapter adapter = new EpisodiosAdapter(listaEpisodio);
            recyclerEpisodio.setAdapter(adapter);

        }catch (JSONException e){
            cargando.hide();
            Toast.makeText(this, "No hay eventos en este día", Toast.LENGTH_SHORT).show();
        }

    }
}
