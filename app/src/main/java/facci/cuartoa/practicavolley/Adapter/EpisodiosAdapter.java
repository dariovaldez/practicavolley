package facci.cuartoa.practicavolley.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import facci.cuartoa.practicavolley.Objetos.Episodio;
import facci.cuartoa.practicavolley.R;

public class EpisodiosAdapter extends RecyclerView.Adapter<EpisodiosAdapter.EpisodioHolder> {

    List<Episodio> listaEpisodios;

    public EpisodiosAdapter(ArrayList<Episodio> listaEpisodio) {
        this.listaEpisodios = listaEpisodio;
    }


    @Override
    public EpisodioHolder onCreateViewHolder(ViewGroup parent, int i) {
        View vista = LayoutInflater.from(parent.getContext()).inflate(R.layout.episodio,parent,false);
        RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        vista.setLayoutParams(layoutParams);
        return new EpisodioHolder(vista);
    }

    @Override
    public void onBindViewHolder(EpisodioHolder episodioHolder, int position) {
        episodioHolder.lbl_idEpisodio.setText(listaEpisodios.get(position).getId());
        episodioHolder.lbl_nameEpisodio.setText(listaEpisodios.get(position).getName());
        episodioHolder.lbl_airDate.setText(listaEpisodios.get(position).getAir_date());
        episodioHolder.lbl_idEpisodio.setText(listaEpisodios.get(position).getEpisode());
    }

    @Override
    public int getItemCount() {
        return listaEpisodios.size();
    }


    public class EpisodioHolder extends RecyclerView.ViewHolder{
        TextView lbl_idEpisodio, lbl_nameEpisodio, lbl_airDate, lbl_Episodio;
        public EpisodioHolder(View itemView) {
            super(itemView);
            lbl_idEpisodio = (TextView) itemView.findViewById(R.id.lbl_idEpisodio);
            lbl_nameEpisodio = (TextView) itemView.findViewById(R.id.lbl_nameEpisodio);
            lbl_airDate = (TextView) itemView.findViewById(R.id.lbl_airDate);
            lbl_Episodio = (TextView) itemView.findViewById(R.id.lbl_Episodio);
        }
    }
}
