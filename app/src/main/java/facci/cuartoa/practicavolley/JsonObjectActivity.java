package facci.cuartoa.practicavolley;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import facci.cuartoa.practicavolley.Objetos.Caracter;

public class JsonObjectActivity extends AppCompatActivity implements Response.Listener<JSONObject>, Response.ErrorListener{

    TextView lbl_id, lbl_name, lbl_gender, lbl_status, lbl_specie;
    ImageView ivPersonaje;
    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;
    ProgressDialog cargando;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_object);
        lbl_id = (TextView) findViewById(R.id.lbl_id);
        lbl_name = (TextView) findViewById(R.id.lbl_name);
        lbl_gender = (TextView) findViewById(R.id.lbl_gender);
        lbl_status = (TextView) findViewById(R.id.lbl_status);
        lbl_specie = (TextView) findViewById(R.id.lbl_specie);
        ivPersonaje = (ImageView) findViewById(R.id.imageViewFoto);

        request = Volley.newRequestQueue(this);

        cargarApiRest();

    }

    private void cargarApiRest() {
        cargando = new ProgressDialog(this);
        cargando.setTitle("Cargando");
        cargando.setMessage("Esto podria tardar un momento");
        cargando.show();

        Intent intent = getIntent();
        final String id = intent.getStringExtra("id");
        
        String url = "https://rickandmortyapi.com/api/character/"+id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        cargando.hide();
        Toast.makeText(this, "No se pudo conectar al servidor", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        try {
            cargando.hide();
            lbl_id.setText("ID: " + response.getInt("id"));
            lbl_name.setText("Name: " + response.getString("name"));
            lbl_specie.setText("Specie: " + response.getString("species"));
            lbl_status.setText("Status: " + response.getString("status"));
            lbl_gender.setText("Gender: " + response.getString("gender"));
            Picasso.get().load(response.getString("image")).into(ivPersonaje);
        } catch (JSONException e) {
            Toast.makeText(this, "No se encontró el personaje", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }
}
