package facci.cuartoa.practicavolley;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnJsonObject, btnJsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnJsonObject = (Button) findViewById(R.id.btnJsonObject);
        btnJsonArray = (Button) findViewById(R.id.btnJsonArray);

        btnJsonObject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), JsonObjectBusquedaActivity.class);
                startActivity(intent);
            }
        });

        btnJsonArray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent intent = new Intent(getApplicationContext(), JsonArrayActivity.class);
                 startActivity(intent);
            }
        });

    }
}
